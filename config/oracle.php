<?php

return [
    'oracle' => [
        'driver'        => 'oracle',
        'tns'           => env('DB_TNS', ''),
        'host'          => env('DB_HOST', 'ORCL'),
        'port'          => env('DB_PORT', '1521'),
        'database'      => env('DB_DATABASE', 'oracle'),
        'username'      => env('DB_USERNAME', 'penjadwalan'),
        'password'      => env('DB_PASSWORD', 'penjadwalan'),
        'charset'       => env('DB_CHARSET', 'AL32UTF8'),
        'prefix'        => env('DB_PREFIX', ''),
        'prefix_schema' => env('DB_SCHEMA_PREFIX', ''),
    ],
];
