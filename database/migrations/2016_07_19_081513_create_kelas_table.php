<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKelasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kelas', function (Blueprint $table) {
            $table->increments('id',10);
            $table->integer('jurusan')->unsigned();
            $table->foreign('jurusan')->references('id')->on('jurusan')->onDelete('cascade');
            $table->integer('prodi')->unsigned();
            $table->foreign('prodi')->references('id')->on('program_studi')->onDelete('cascade');
            $table->string('pararel',1);
            $table->string('kode_kelas',10);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('kelas');
    }
}
