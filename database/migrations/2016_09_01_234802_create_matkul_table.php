<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatkulTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mata_kuliah', function (Blueprint $table) {
            $table->increments('id',10);
            $table->string('kode',20);
            $table->string('pararel',2);
            $table->string('mata_kuliah',100);
            $table->integer('jam',2);
            $table->integer('sks',2);
            $table->integer('tahun',4);
            $table->integer('semester',1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mata_kuliah');
    }
}
