<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerkuliahTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kuliah', function (Blueprint $table) {
            $table->increments('id',20);
            $table->integer('matkul_id')->unsigned();
            $table->foreign('matkul_id')->references('id')->on('mata_kuliah')->onDelete('cascade');
            $table->integer('tahun',4);
            $table->integer('semester',1);
            $table->integer('dosen_id')->unsigned();
            $table->foreign('dosen_id')->references('id')->on('pegawai')->onDelete('cascade');
            $table->string('teknisi',50);
            $table->string('hari',50);
            $table->string('jam',1);
            $table->string('hari_2',50);
            $table->string('jam_2',1);
            $table->integer('ruang_id')->unsigned();
            $table->foreign('ruang_id')->references('id')->on('ruang')->onDelete('cascade');
            $table->integer('kelas_id')->unsigned();
            $table->foreign('kelas_id')->references('id')->on('kelas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('kuliah');
    }
}
