<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Model_Prodi extends Model
{
    protected $table = 'program_studi';
    protected $primaryKey = 'nomor';
    protected $fillable = ['program_studi'];
    public $timestamps = false;
}
