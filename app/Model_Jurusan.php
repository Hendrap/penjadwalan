<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Model_Jurusan extends Model
{
    protected $table = 'jurusan';
    protected $primaryKey = 'nomor';
    protected $fillable = ['jurusan'];
    public $timestamps = false;
}
