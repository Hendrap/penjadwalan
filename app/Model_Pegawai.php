<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Model_Pegawai extends Model
{
    protected $table = 'pegawai';
    protected $primaryKey = 'id';
    protected $fillable = ['nama','nip','staff'];
    public $timestamps = false;

}
