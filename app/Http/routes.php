<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/pegawai', 'Pegawai@index');
Route::get('/pegawai/add', 'Pegawai@create');
Route::post('/pegawai/store', 'Pegawai@store');
Route::get('/pegawai/edit/{nomor}', 'Pegawai@edit');
Route::post('/pegawai/update/{nomor}', 'Pegawai@update');
Route::get('/pegawai/delete/{nomor}', 'Pegawai@destroy');
Route::get('/pegawai/import', 'Pegawai@import');
Route::get('/pegawai/upload', 'Pegawai@upload');


Route::get('test', function() {
    dd (DB::connection()->getPdo());
});

Route::get('check', function() {
    dd(DB::connection('penjadwalan')->getPdo());
});
Route::get('/jurusan', 'Jurusan@index');
Route::get('/prodi', 'Program_studi@index');
Route::get('/kelas', 'Kelas@index');

Route::get('/kuliah', 'Kuliah@index');
