<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Model_Pegawai;
class Pegawai extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = Model_Pegawai::orderBy('id','DESC')->get();
        return view('pegawai/view')->with('datas', $datas);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $query['tabel'] = "pegawai";
        $query['select'] = Model_Pegawai::orderBy('id','DESC')->get();
        return view('pegawai/add')->with('datas',$query);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
              'nama' => 'required',
              'nip' => 'required'
        ]);

             $tambah = new Model_Pegawai();
             $tambah->nama = $request['nama'];
             $tambah->nip = $request['nip'];
             $tambah->staff = $request['staff'];
             $tambah->save();

             return redirect()->to('/pegawai');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $query['tabel'] = "pegawai";
        $query['select'] = Model_Pegawai::orderBy('id','DESC')->get();
        $query['edit'] = Model_Pegawai::where('id',$id)->first();
        return view('pegawai/edit')->with('datas',$query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = Model_Pegawai::where('id', $id)->first();
        $update->nama = $request['nama'];
        $update->nip = $request['nip'];
        $update->staff = $request['staff'];
        $update->update();

        return redirect()->to('/pegawai');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hapus = Model_Pegawai::find($id);
        $hapus->delete();

        return redirect()->to('/pegawai');
    }

}
