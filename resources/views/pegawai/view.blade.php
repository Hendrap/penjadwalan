@extends('layouts.tabel')
@section('content')

<section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>NIP</th>
                  <th>Nama</th>
                  <th>Staff</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($datas as $data)
                <tr>
                  <td>{{ $data->nip }}</td>
                  <td>{{ $data->nama }}</td>
                  <td>{{ $data->staff }}</td>
                  <td>
                    <div class="btn-group">
                    <button type="button" class="btn btn-default">Action</button>
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                      <span class="caret"></span>
                      <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Disable</a></li>
                      <li><a href="{{url('pegawai/edit',$data->nomor)}}">Edit</a></li>
                      <li><a href="{{url('pegawai/delete',$data->nomor)}}">hapus</a></li>
                    </ul>
                    </div>
                  </td>
                </tr>
                @endforeach
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@endsection