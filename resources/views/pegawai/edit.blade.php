@extends('layouts.form')
@section('content')
<section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title"> {{$datas['tabel']}}</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{url('pegawai/update',$datas['edit']->nomor)}}" method="POST">
              <div class="box-body">
                {!! csrf_field() !!}
                <div class="form-group">
                  <label for="inputLabel">Nama</label>
                  <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Pegawai" value="{{$datas['edit']->nama}}">
                </div>
                <div class="form-group">
                  <label for="inputLabel">NIP</label>
                  <input type="text" class="form-control" id="nip" name="nip" placeholder=""  value="{{$datas['edit']->nip}}">
                </div>
                <div class="form-group">
                <label>Staff</label>
                <select class="form-control select2" style="width: 100%;" name="staff">
                  <option selected="selected"  value="{{$datas['edit']->nama}}"></option>
                  @foreach($datas['select'] as $data)
                  <option value="{{$data->nomor}}">{{$data->nama}}</option>
                  @endforeach
                </select>
              </div>

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->
        </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Visit <a href="https://select2.github.io/">Select2 documentation</a> for more examples and information about
          the plugin.
        </div>
      </div>
      <!-- /.box -->
    </section>
@endsection
