@extends('layouts.tabel')
@section('content')

<section class="content">
      <div class="row">
        <div class="col-xs-12">
          

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Table With Full Features</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Jam ke-</th>
                  <th>Senin</th>
                  <th>Selasa</th>
                  <th>Rabu</th>
                  <th>Kamis</th>
                  <th>Jum`at</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <td>1</td>
                  <td>
                    Mata Kuliah <br/>
                    Nama Dosen <br/>
                    Ruang <br/>
                  </td>
                  <td>
                    Mata Kuliah <br/>
                    Nama Dosen <br/>
                    Ruang <br/>
                  </td>
                  <td>
                    Mata Kuliah <br/>
                    Nama Dosen <br/>
                    Ruang <br/>
                  </td>
                  <td>
                    Mata Kuliah <br/>
                    Nama Dosen <br/>
                    Ruang <br/>
                  </td>
                  <td>
                    Mata Kuliah <br/>
                    Nama Dosen <br/>
                    Ruang <br/>
                  </td>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>

@endsection